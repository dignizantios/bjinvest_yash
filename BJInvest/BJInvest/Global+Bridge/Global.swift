//
//  Global.swift
//  BJInvest
//
//  Created by Abhay on 01/01/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import Foundation
import UIKit
import MaterialComponents
import Alamofire
import SwiftyJSON

//MARK:- PrintFont

func printFonts()
{
    let fontFamilyNames = UIFont.familyNames
    for familyName in fontFamilyNames {
        print("------------------------------")
        print("Font Family Name = [\(familyName)]")
        let names = UIFont.fontNames(forFamilyName: familyName)
        print("Font Names = [\(names)]")
    }
}

//MARK:- Model object

var objUser = User()

//MARK:- Timezone

var localTimeZoneName: String { return TimeZone.current.identifier }

// Device type

var strDeviceType = "1"

// Storyboards
let objStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)

// common string

let StringFilePath = Bundle.main.path(forResource: "CommonString", ofType: "plist")
let dictStrings = NSDictionary(contentsOfFile: StringFilePath!)

// validation string

let validationFilePath = Bundle.main.path(forResource: "Validation", ofType: "plist")
let validationStrings = NSDictionary(contentsOfFile: validationFilePath!)


// get common string
func getCommonString(key:String) -> String
{
    return dictStrings?.object(forKey: key) as? String ?? ""
}

//get Validaion
func getValidationString(key:String) -> String
{
    return validationStrings?.object(forKey: key) as? String ?? ""
}

//MARK: - Toast
func makeToast(message : String){
    
    let messageSnack = MDCSnackbarMessage()
    messageSnack.text = message
    MDCSnackbarManager.show(messageSnack)
}

//MARK:- Constant

let useDefualt = UserDefaults.standard
var summurySelectionList:[JSON] = []
var summuryCheckedList:[JSON] = []


//MARK:-  Loader Property

let Defaults = UserDefaults.standard
var strLoader:String = ""
var LoaderType:Int = 23
var Loadersize = CGSize(width: 30, height: 30)
