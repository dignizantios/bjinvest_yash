//
//  UIApplication+Extension.swift
//  Muber
//
//  Created by Abhay on 22/03/18.
//

import Foundation
import UIKit

//MARK: - Application Extension
extension UIApplication
{
    var statusBarView: UIView? {
        UIApplication.shared.statusBarStyle = .lightContent
        return value(forKey: "statusBar") as? UIView
    }
}
extension AppDelegate
{
    var statusBarView: UIView? {
        UIApplication.shared.statusBarStyle = .lightContent
        return value(forKey: "statusBar") as? UIView
    }
}
