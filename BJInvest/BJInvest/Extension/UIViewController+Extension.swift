//
//  UIViewController+Extension.swift
//  Muber
//
//  Created by Abhay on 22/03/18.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import NVActivityIndicatorView

extension UIViewController:NVActivityIndicatorViewable
{
    
    //MARK: - StartAnimating
    
    func showLoader()
    {
        startAnimating(Loadersize, message:strLoader , type: NVActivityIndicatorType(rawValue:LoaderType))
    }
    
    func stopLoader()
    {
        self.stopAnimating()
    }
    
    //MARK: - Print All Fonts
    
    func printFonts()
    {
        let fontFamilyNames = UIFont.familyNames
        for familyName in fontFamilyNames {
            print("------------------------------")
            print("Font Family Name = [\(familyName)]")
            let names = UIFont.fontNames(forFamilyName: familyName )
            print("Font Names = [\(names)]")
        }
    }
    
    //MARK: - SetUpNaviagtionBar
    
    
    
    func setupNavigationBarWithBackButton(titleText:String)
    {
        self.edgesForExtendedLayout = UIRectEdge.init(rawValue: 0)
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        
       /* let leftImage  = UIImage(named: "ic_back_arrow_white_header")!.withRenderingMode(.alwaysOriginal)
        var leftButton = UIButton(frame: CGRect(x:0,y: 0,width: 20,height: 44))
        leftButton = UIButton(type: .custom)
        leftButton.tintColor = .white
        leftButton.setBackgroundImage(leftImage, for: .normal)
        leftButton.addTarget(self, action: #selector(backButtonAction), for:.touchUpInside)
         
         let menuButton = UIBarButtonItem(customView: leftButton)
         self.navigationItem.leftBarButtonItem = menuButton
         self.navigationItem.hidesBackButton = false
         
         */
        let leftButton = UIBarButtonItem(image: UIImage(named: "ic_back_header"), style: .plain, target: self, action: #selector(backButtonAction))
        leftButton.tintColor = UIColor.black
        self.navigationItem.leftBarButtonItem = leftButton
        self.navigationItem.hidesBackButton = false

        let HeaderView = UILabel.init(frame: CGRect(x: 0, y: 0, width: 40, height: 19))
        HeaderView.isUserInteractionEnabled = false
        HeaderView.text = titleText
        print("titleText-->",titleText)
        HeaderView.font = themeFont(size: 18, fontname: .semibold)
        HeaderView.textColor = UIColor.black
        self.navigationItem.titleView = HeaderView
        
        self.navigationItem.hidesBackButton = true
        self.navigationItem.hidesBackButton = true
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.clear.cgColor
        
    }
    
    func setupNavigationBar(titleText:String)
    {
        self.edgesForExtendedLayout = UIRectEdge.init(rawValue: 0)
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor.black
        
        
        
        /* let leftImage  = UIImage(named: "ic_back_arrow_white_header")!.withRenderingMode(.alwaysOriginal)
         var leftButton = UIButton(frame: CGRect(x:0,y: 0,width: 20,height: 44))
         leftButton = UIButton(type: .custom)
         leftButton.tintColor = .white
         leftButton.setBackgroundImage(leftImage, for: .normal)
         leftButton.addTarget(self, action: #selector(backButtonAction), for:.touchUpInside)
         
         let menuButton = UIBarButtonItem(customView: leftButton)
         self.navigationItem.leftBarButtonItem = menuButton
         self.navigationItem.hidesBackButton = false
         
 
        let rightButton = UIBarButtonItem(image: UIImage(named: "ic_more_header"), style: .plain, target: self, action: #selector(backButtonAction))
        leftButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = leftButton */
        
        let HeaderView = UILabel.init(frame: CGRect(x: 0, y: 0, width: 40, height: 19))
        HeaderView.isUserInteractionEnabled = false
        HeaderView.text = titleText
        HeaderView.font = themeFont(size: 18, fontname: .semibold)
        HeaderView.textColor = UIColor.black
        self.navigationItem.titleView = HeaderView
        
        self.navigationItem.hidesBackButton = true
        
    }
    
    @objc func backButtonAction()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    
    
    @objc func doneButtonAction(textfield:UITextField)
    {
        self.view.endEditing(true)
    }
    
    
    //MARK: - Image Choose Methods
    
    func ShowChooseImageOptions(picker : UIImagePickerController)
    {
        let alertController = UIAlertController(title: "", message: getCommonString(key: "Choose_Option_key"), preferredStyle: .actionSheet)
        
        let cameraButton = UIAlertAction(title: getCommonString(key: "Take_a_photo_key"), style: .default, handler: { (action) -> Void in
            print("camera button tapped")
            self.openCamera(picker : picker)
        })
        
        let  galleryButton = UIAlertAction(title: getCommonString(key: "Select_from_Photos_key"), style: .default, handler: { (action) -> Void in
            
            print("gallery button tapped")
            self.openGallary(picker : picker)
        })
        let  dismissButton = UIAlertAction(title: getCommonString(key: "Cancel_key"), style: .cancel, handler: { (action) -> Void in
            
        })
        
        alertController.addAction(cameraButton)
        alertController.addAction(galleryButton)
        alertController.addAction(dismissButton)
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    func openGallary(picker : UIImagePickerController)
    {
        //  picker.allowsEditing = true
        picker.sourceType = UIImagePickerController.SourceType.photoLibrary
        present(picker, animated: true, completion: nil)
    }
    
    
    func openCamera(picker : UIImagePickerController)
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            //  picker.allowsEditing = true
            picker.sourceType = UIImagePickerController.SourceType.camera
            picker.cameraCaptureMode = .photo
            present(picker, animated: true, completion: nil)
        }else{
            let alert = UIAlertController(title:"" , message: "Device not support to take photos", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK:- pop To Specific
    
    func navigateToViewcontroller()  {
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
        for aViewController in viewControllers {
            if aViewController is ViewController {
                self.navigationController!.popToViewController(aViewController, animated: true)
            }
        }
    }
    
    //MARK:- userdefault 
    
    func getUserDetail(_ forKey: String) -> String
    {
        guard let userDetail = UserDefaults.standard.value(forKey: "userDetail") as? Data else { return "" }
        let data = JSON(userDetail)
        //print(data)
        return data[forKey].stringValue
    }
    
    func getPuhNotification(_ forKey: String) -> String
    {
        guard let userDetail = UserDefaults.standard.value(forKey: "NotificationDic") as? Data else { return "" }
        let data = JSON(userDetail)
        //print(data)
        return data[forKey].stringValue
    }
   
    
    
    @objc func userLogout()
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(kBasicURL)\(kUserLogout)"
            
            print("URL: \(url)")
            
            let param =  ["lang":strLang,
                          "user_id":getUserDetail("user_id"),
                          "access_token":getUserDetail("access_token"),
                          "device_token":"",
                          "timezone":localTimeZoneName
                         ]
            
            print("Param : \(param)")
            
            showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.stopLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == strSuccessResponse
                    {
                        Defaults.removeObject(forKey: "userDetail")
                        Defaults.synchronize()
                        
                        self.navigateToViewcontroller()
                    }
                    else
                    {
                        makeToast(message: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    makeToast(message: getCommonString(key: "Something_went_wrong_key"))
                }
            }
            
        }
        else
        {
            makeToast(message: getCommonString(key: "No_internet_connection_key"))
        }
    }
}
