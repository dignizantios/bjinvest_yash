//
//  UiView+Extenstion.swift
//  MuberSP
//
//  Created by DK on 18/05/18.
//  Copyright © 2018 Abhay. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    func viewNextPresentingViewController() -> UIViewController? {
        var theController = self.next
        
        while theController != nil && !(theController?.isKind(of: UIViewController.classForCoder()))! {
            theController = theController?.next
        }
        
        return theController as? UIViewController
    }
}
