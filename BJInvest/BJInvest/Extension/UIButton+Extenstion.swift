//
//  UIButton+Extenstion.swift
//  BJInvest
//
//  Created by Abhay on 02/01/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import Foundation
import UIKit

extension UIButton
{
    func setUpThemeButtonUI(fontColor:UIColor,size:Float,type:themeFonts,title:String,bgcolor:UIColor)
    {
        self.setTitle(title, for: .normal)
        self.setTitleColor(fontColor, for: .normal)
        self.titleLabel?.font = themeFont(size: size, fontname: type)
        self.backgroundColor = bgcolor
    }
}
