
//
//  UIFont+Extension.swift
//  BJInvest
//
//  Created by Abhay on 01/01/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import Foundation
import UIKit

enum themeFonts : String
{
    case black = "Muli-Black"
    case light = "Muli-Light"
    case blackItalic = "Muli-BlackItalic"
    case italic = "Muli-Italic"
    case extraLight = "Muli-ExtraLight"
    case boldItalic = "Muli-BoldItalic"
    case semibold = "Muli-SemiBold"
    case regular = "Muli-Regular"
    case extraBoldItalic = "Muli-ExtraBoldItalic"
    case extraBold = "Muli-ExtraBold"
    case extraLightItalic = "Muli-ExtraLightItalic"
    case semiBoldItalic = "Muli-SemiBoldItalic"
    case lightItalic = "Muli-LightItalic"
    case bold = "Muli-Bold"
}


func themeFont(size : Float,fontname : themeFonts) -> UIFont
{
    if UIScreen.main.bounds.width <= 320
    {
        return UIFont(name: fontname.rawValue, size: CGFloat(size) - 2.0)!
    }
    else
    {
        return UIFont(name: fontname.rawValue, size: CGFloat(size))!
    }
    
}
