//
//  String+Extension.swift
//  MuberSP
//
//  Created by Abhay on 15/05/18.
//  Copyright © 2018 Abhay. All rights reserved.
//

import Foundation
import UIKit

extension String {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: Data(utf8),
                                          options: [.documentType: NSAttributedString.DocumentType.html,
                                                    .characterEncoding: String.Encoding.utf8.rawValue],
                                          documentAttributes: nil)
        } catch {
            print("error: ", error)
            return nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
        
    func attributedStringForPartiallyColoredText(_ textToFind: String, with color: UIColor) -> NSMutableAttributedString
    {
            let mutableAttributedstring = NSMutableAttributedString(string: self)
            let range = mutableAttributedstring.mutableString.range(of: textToFind, options: .caseInsensitive)
            if range.location != NSNotFound {
                mutableAttributedstring.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range)
            }
            return mutableAttributedstring
    }
    
    
}
