//
//  MainServicesListVC.swift
//  BJInvest
//
//  Created by Haresh Bhai on 03/01/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import UIKit
import CarbonKit
import SwiftyJSON
import Alamofire
import AlamofireSwiftyJSON


class MainServicesListVC: UIViewController,CarbonTabSwipeNavigationDelegate {
    
    //MARK:- Outlets
    
    @IBOutlet weak var MainView: UIView!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnNextButtonHeightConstraint: NSLayoutConstraint!
    
    //MARK:- Delcaration
    
    var arrProduct:[JSON] = []
    var arrMenu = [String]()
    var carbonTabSwipeNavigation = CarbonTabSwipeNavigation()
    var selectedIndexVC = 0
    var items = NSArray()
    var currentJson = JSON()
   // var currentJsonList:[JSON] = []
    var rightButton : UIButton = UIButton()
    var isNonListValue = Bool()
    var selectionCheckBoxList:[[Selection]] = []
    var editSelection = ""
    var isEdited = Bool()
    
    //MARK:- View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.appThemeBackgroundColor
        
        getCategoryList()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        
        setupNavigationBar()
        
        btnNextButtonHeightConstraint.constant = 0.0
        btnNext.setUpThemeButtonUI(fontColor: UIColor.white, size: 16, type: .regular, title: getCommonString(key: "Next_key").uppercased(), bgcolor: UIColor.appThemeGreenColor)
        self.view.addSubview(btnNext)
    }
    
    
    func decoreTabBar()
    {
        
        carbonTabSwipeNavigation.setTabExtraWidth(40)
        carbonTabSwipeNavigation.setIndicatorColor(UIColor.black)
        carbonTabSwipeNavigation.setNormalColor(UIColor.appThemeLightGrayColor)
        
        carbonTabSwipeNavigation.setSelectedColor(UIColor.black)
       // carbonTabSwipeNavigation.insert(intoRootViewController: self)
        let color: UIColor = .white
        self.navigationController?.navigationBar.isTranslucent = false
        
        carbonTabSwipeNavigation.toolbar.isTranslucent = false
        carbonTabSwipeNavigation.toolbar.barTintColor = color
        carbonTabSwipeNavigation.toolbar.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        carbonTabSwipeNavigation.toolbar.layer.shadowOffset = CGSize(width: 0, height: 2)
        carbonTabSwipeNavigation.toolbar.layer.shadowOpacity = 0.7
        carbonTabSwipeNavigation.toolbar.layer.masksToBounds = false
        carbonTabSwipeNavigation.toolbar.layer.shadowRadius = 1.0
        carbonTabSwipeNavigation.isEditing = false
        carbonTabSwipeNavigation.carbonTabSwipeScrollView.isScrollEnabled = false
        carbonTabSwipeNavigation.setNormalColor(UIColor.lightGray, font:themeFont(size: 15, fontname: .light))
        
        carbonTabSwipeNavigation.setSelectedColor(UIColor.black, font: themeFont(size: 15, fontname: .regular))
        
        carbonTabSwipeNavigation.toolbar.shadowImage(forToolbarPosition: .bottom)
       // carbonTabSwipeNavigation.pagesScrollView!.isScrollEnabled = false
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
        if isEdited{
            
            for (i,item) in items.enumerated()
            {
                let currentItem = item as! String
                print("currentItem-->",currentItem)
                print("editSelection-->",editSelection)
                if editSelection.lowercased() == currentItem.lowercased()
                {
                    carbonTabSwipeNavigation.setCurrentTabIndex(UInt(i), withAnimation: true)
                    isEdited = false
                    break
                }
                
            }
            
        }
        // carbonTabSwipeNavigation.setCurrentTabIndex(1, withAnimation: true)
    }
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        
        selectedIndexVC = Int(index)       
        guard let storyboard = storyboard else { return UIViewController() }
        switch arrProduct[selectedIndexVC]["type"].intValue {
        case 1:
            let vc = storyboard.instantiateViewController(withIdentifier: "OtherSelectingListVC") as! OtherSelectingListVC
            vc.nextButtonDelegate = self
            vc.currentVisibleIndex = selectedIndexVC
           // vc.curentIndexName = items[Int(index)] as! String
            vc.dictData =  arrProduct[selectedIndexVC]
            return vc
        case 2:
            let vc = storyboard.instantiateViewController(withIdentifier: "ServiceListVC") as! ServiceListVC
            vc.nextButtonDelegate = self
            vc.currentVisibleIndex = selectedIndexVC
          //  vc.curentIndexName = items[Int(index)] as! String
            vc.dictData =  arrProduct[selectedIndexVC]
            return vc
            
        default:
            let vc = storyboard.instantiateViewController(withIdentifier: "ServiceListVC") as! ServiceListVC
            vc.nextButtonDelegate = self
            vc.currentVisibleIndex = selectedIndexVC
           // vc.curentIndexName = items[Int(index)] as! String
            vc.dictData =  arrProduct[selectedIndexVC]
            return vc
        }
    }
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, didMoveAt index: UInt) {
        
        selectedIndexVC = Int(index)
        
        //        let vc = carbonTabSwipeNavigation.viewControllers[index] as! ProfileVc
        
        //   if scrollView.contentOffset.y == self.viewDetail.bounds.height {
        //        vc.tblPoolList.isScrollEnabled = true
        //        vc.tblPoolList.contentOffset = CGPoint.zero
        
    }
    func setupNavigationBar()
    {
        self.edgesForExtendedLayout = UIRectEdge.init(rawValue: 0)
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        
         let Image  = UIImage(named: "ic_logo_header")
        
        rightButton.setTitle(getCommonString(key: "Done_key"), for: .normal)
        
        rightButton.addTarget(self, action: #selector(manageDoneButton), for: .touchUpInside)
        rightButton.setTitleColor(UIColor.black, for: .normal)
        rightButton.alpha = 0.0
        rightButton.isUserInteractionEnabled = false
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: rightButton)
        
        
        let HeaderView = UILabel.init(frame: CGRect(x: 0, y: 0, width: 40, height: 19))
        HeaderView.isUserInteractionEnabled = false
        HeaderView.text = ""
        HeaderView.font = themeFont(size: 18, fontname: .semibold)
        HeaderView.textColor = UIColor.white
        self.navigationItem.titleView = UIImageView(image: Image)
        
        self.navigationItem.hidesBackButton = true
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.clear.cgColor
        
    }
    //MARK:- ButtonAction
    
    @IBAction func btnNextAction(_ sender: UIButton) {
        btnNextButtonHeightConstraint.constant = 0.0
        let currentIndex = selectedIndexVC
        let nxtIndex = selectedIndexVC+1
        /*if currentIndex == items.count-1{
            rightButton.alpha = 1
            rightButton.isUserInteractionEnabled = true
        }else{
            rightButton.alpha = 0
            rightButton.isUserInteractionEnabled = false
        }*/
        
        
        if isNonListValue
        {
            for (_,item) in selectionCheckBoxList.enumerated()
            {
                for i in 0...item.count-1{
                    var selection = item[i].tempSelectionJSN!
                    let isSelected = selection["isSelected"].boolValue
                    if isSelected{
                        if let currentdic = selection as? JSON{
                            print("currentIndex-->",currentIndex)
                            print("currentdic-->",currentdic)
                            summuryCheckedList.append(currentdic)
                        }
                    }
                }
            }
            print("summuryCheckedList-->",summuryCheckedList)
            carbonTabSwipeNavigation.setCurrentTabIndex(UInt(nxtIndex), withAnimation: true)
        }else{
            print("currentIndex-->",currentIndex)
            if let currentdic = currentJson as? JSON{
                var index = -1
                let name = arrProduct[selectedIndexVC]["category_id"].stringValue
                if summurySelectionList.contains(where: { (val) -> Bool in
                    if val["category_id"].stringValue == name
                    {
                        index = summurySelectionList.firstIndex(of: val) ?? -1
                        return true
                    }
                    return false
                })
                {
                    print("Index - ",index)
                    summurySelectionList[index] = currentdic
                }else{
                    print("Not found")
                    summurySelectionList.append(currentdic)
                    
                }
                carbonTabSwipeNavigation.setCurrentTabIndex(UInt(nxtIndex), withAnimation: true)
            }
            
        }
        print("summurySelectionList.count-->",summurySelectionList.count)
        print("summuryCheckedList.count-->",summuryCheckedList.count)
        print("items.count-->",items.count)
        
        var  totalCnt = summuryCheckedList.count > 0 ? 1 : 0
        totalCnt = summurySelectionList.count+totalCnt
        print("totalCnt-->",totalCnt)
        if totalCnt == items.count{
            rightButton.alpha = 1
            rightButton.isUserInteractionEnabled = true
        }        
       
    }
    @objc func manageDoneButton()
    {
        var  totalCnt = summuryCheckedList.count > 0 ? 1 : 0
        totalCnt = summurySelectionList.count+totalCnt
        print("totalCnt-->",totalCnt)
        if totalCnt == items.count{
            let SummeryVC = self.storyboard?.instantiateViewController(withIdentifier: "SummeryVC") as! SummeryVC
            self.navigationController?.pushViewController(SummeryVC, animated: true)
        }else{
            let notSelectAll = getValidationString(key: "Select_one_value_in_all_selection_list_key")
            makeToast(message: notSelectAll)
        }
        
    }

}
extension MainServicesListVC : SelectionNextButtonDelegate
{
    func HideNextButton() {
        btnNextButtonHeightConstraint.constant = 0.0
        currentJson = JSON()
        selectionCheckBoxList.removeAll()
    }
    
    func NextButtonVisibleNonListAction(dic: [[Selection]]) {
        btnNextButtonHeightConstraint.constant = 40.0
        currentJson = JSON()
        selectionCheckBoxList = dic
        isNonListValue = true
    }
    
    func NextButtonVisibleAction(dic: JSON) {
        btnNext.isHidden = false
        btnNextButtonHeightConstraint.constant = 40.0
        currentJson = JSON()
        currentJson = dic
        selectionCheckBoxList.removeAll()
        isNonListValue = false
    }
}


//MARK:- Service

extension MainServicesListVC
{
    func getCategoryList()
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(kBasicURL)\(kCategoryList)"
            
            print("URL: \(url)")
            
            let param =  ["lang":strLang,
                          "user_id":getUserDetail("user_id"),
                          "access_token":getUserDetail("access_token")
            ]
            
            print("Param : \(param)")
            
            showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.stopLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == strSuccessResponse
                    {
                       
                        self.arrProduct = []
                        self.arrProduct = json["data"].arrayValue
                        self.arrMenu = []
                        for i in 0..<self.arrProduct.count
                        {
                            let dict = self.arrProduct[i]
                            self.arrMenu.append(dict["category_name"].stringValue)
                        }
                        self.carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: self.arrMenu , delegate: self)
                        self.carbonTabSwipeNavigation.insert(intoRootViewController: self, andTargetView: self.MainView)
                        
                         self.decoreTabBar()
                    }
                    else
                    {
                        makeToast(message: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    makeToast(message: getCommonString(key: "Something_went_wrong_key"))
                }
            }
            
        }
        else
        {
            makeToast(message: getCommonString(key: "No_internet_connection_key"))
        }
    }
    
}
