//
//  ServiceListVC.swift
//  BJInvest
//
//  Created by Haresh Bhai on 03/01/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import SDWebImage

struct Selection {
    
    var tempSelectionJSN: JSON?
    var tempSelection: Bool?
    
    init(SelectionJSN: JSON, isSelection: Bool) {
        tempSelectionJSN = SelectionJSN
        tempSelection = isSelection
    }
}
protocol SelectionNextButtonDelegate  {
    
    func NextButtonVisibleAction(dic:JSON)
    
    func NextButtonVisibleNonListAction(dic:[[Selection]])
    
    func HideNextButton()
    
    
}

class ServiceListVC: UIViewController {
    
    //MARK:- Outlets
    
    @IBOutlet weak var tblViewSelectingList: UITableView!
    
    //MARK:- Declaration
    var selectionList:[Selection] = []
    var nextButtonDelegate : SelectionNextButtonDelegate?
    var currentVisibleIndex = Int()
    var curentIndexName = String()
    var dictData = JSON()
    var arrCatInfo:[JSON] = []
    var strMsg = String()
    
    //MARK:- View lifecycle
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        getCategoryInfo()
        self.view.backgroundColor = UIColor.appThemeBackgroundColor
        tblViewSelectingList.delegate = self
        tblViewSelectingList.dataSource = self
        //decoreUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.tblViewSelectingList.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    //MARK:- Custom Methods
    
    func decoreUI(){
        
        var selection = JSON()
        selection["categoryId"].stringValue = curentIndexName
        selection["img"] = "image_placeholder_main_list.png"
        selection["title"] = "Wood Pattern PE Mat - Brown"
        selection["price"] = "20,000"
        selection["isListing"] = "true"
        selection["isSelected"] = "false"
        let selectionobj = Selection.init(SelectionJSN: selection, isSelection: false)
        selectionList.append(selectionobj)
        selection = JSON()
        selection["categoryId"].stringValue = curentIndexName
        selection["img"] = "image_placeholder_main_list.png"
        selection["title"] = "Wood Pattern PE Mat - Dark Brown"
        selection["price"] = "20,000"
        selection["isListing"] = "true"
        selection["isSelected"] = "false"
        let selectionobj1 = Selection.init(SelectionJSN: selection, isSelection: false)
        selectionList.append(selectionobj1)
        selection = JSON()
        selection["categoryId"].stringValue = curentIndexName
        selection["img"] = "image_placeholder_main_list.png"
        selection["title"] = "Laminate Flooring Light Yellow"
        selection["price"] = "20,000"
        selection["isListing"] = "true"
        selection["isSelected"] = "false"
        let selectionobj2 = Selection.init(SelectionJSN: selection, isSelection: false)
        selectionList.append(selectionobj2)
        tblViewSelectingList.reloadData()
    }
    


}
//MARK:- UITableView delegate datasource methods
extension ServiceListVC : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrCatInfo.count == 0
        {
            let lbl = UILabel()
            lbl.text = strMsg
            lbl.font = themeFont(size: 20, fontname: .regular)
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = UIColor.black
            lbl.center = tableView.center
            tableView.backgroundView = lbl
            return 0
        }
        tableView.backgroundView = nil
        return arrCatInfo.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectingListCell", for: indexPath as IndexPath) as! SelectingListCell
      //  let selection = selectionList[indexPath.row].tempSelectionJSN!
        let dict = arrCatInfo[indexPath.row]
        cell.setData(data: dict)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
      /*  for i in 0...selectionList.count-1{
            var SelectionJSN = selectionList[i].tempSelectionJSN!
            SelectionJSN["isSelected"].stringValue = "false"
            selectionList[i].tempSelectionJSN! = SelectionJSN
        }
        var selection = selectionList[indexPath.row].tempSelectionJSN!
        selection["isSelected"].stringValue = "true"
        selectionList[indexPath.row].tempSelectionJSN! = selection
        //tblViewSelectingList.reloadRows(at: [indexPath], with: .none)
        self.tblViewSelectingList.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 40, right: 0)
        tblViewSelectingList.reloadData()
        nextButtonDelegate?.NextButtonVisibleAction(dic: selectionList[indexPath.row].tempSelectionJSN!)*/
        
        for i in 0..<self.arrCatInfo.count
        {
            var dict = self.arrCatInfo[i]
            dict["is_selected"] = "false"
            self.arrCatInfo[i] = dict
        }
        
        var dict = self.arrCatInfo[indexPath.row]
        dict["is_selected"] = "true"
        self.arrCatInfo[indexPath.row] = dict
        self.tblViewSelectingList.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 40, right: 0)
        tblViewSelectingList.reloadData()
        nextButtonDelegate?.NextButtonVisibleAction(dic: self.arrCatInfo[indexPath.row] )
        
    }
    
}

//MARK:- Service

extension ServiceListVC
{
    func getCategoryInfo()
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(kBasicURL)\(kCategoryInfoData)"
            
            print("URL: \(url)")
            
            let param =  ["lang":strLang,
                          "user_id":getUserDetail("user_id"),
                          "access_token":getUserDetail("access_token"),
                          "category_id":dictData["category_id"].stringValue
            ]
            
            print("Param : \(param)")
            
            showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.stopLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == strSuccessResponse
                    {
                        
                        self.arrCatInfo = []
                        self.arrCatInfo = json["data"].arrayValue
                       
                        for i in 0..<self.arrCatInfo.count
                        {
                            var dict = self.arrCatInfo[i]
                            dict["is_selected"] = "false"
                            self.arrCatInfo[i] = dict
                        }
                    }
                    else
                    {
                        self.arrCatInfo = []
                        self.strMsg = json["msg"].stringValue
                        makeToast(message: json["msg"].stringValue)
                    }
                    self.tblViewSelectingList.reloadData()
                    
                }
                else
                {
                    makeToast(message: getCommonString(key: "Something_went_wrong_key"))
                }
            }
            
        }
        else
        {
            makeToast(message: getCommonString(key: "No_internet_connection_key"))
        }
    }
    
}

