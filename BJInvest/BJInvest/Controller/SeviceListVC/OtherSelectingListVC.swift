//
//  OtherSelectingListVC.swift
//  BJInvest
//
//  Created by Haresh Bhai on 05/01/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON

struct OtherSelection {
    
    var tempSelectionJSN: JSON?
    var tempSelection: Bool?
    
    init(SelectionJSN: JSON, isSelection: Bool) {
        tempSelectionJSN = SelectionJSN
        tempSelection = isSelection
    }
}



class OtherSelectingListVC: UIViewController {

    //MARK:- Outlets
    
    @IBOutlet weak var lblModel: UILabel!
    @IBOutlet weak var tblViewSelectingList: UITableView!
    
    //MARK:- Declaration
    
    var selectionList:[[Selection]] = []
    var selections:[Selection] = []
    var nextButtonDelegate : SelectionNextButtonDelegate?
    var currentVisibleIndex = Int()
    var currentIndexName = String()
    var dictData = JSON()
    var arrCategoryList:[JSON] = []
    var strMsg = String()
    
    //MARK:- View lifecycle
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.view.backgroundColor = UIColor.appThemeBackgroundColor
        tblViewSelectingList.delegate = self
        tblViewSelectingList.dataSource = self
        //getCategoryInfo()
        decoreUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.tblViewSelectingList.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    //MARK:- Custom Methods
    
    func decoreUI(){
        lblModel.text = getCommonString(key: "Model_key").uppercased()
        lblModel.textColor = UIColor.black
        lblModel.font = themeFont(size: 16, fontname: .semibold)
        
        var selection = JSON()
        selection["categoryId"].stringValue = currentIndexName
        selection["id"] = "1"
        selection["title"] = "Remove the freezer and replace wit a combination / freezer and a cabnet of 6900"
        selection["isListing"] = "false"
        selection["isSelected"] = "false"
        let selectionobj = Selection.init(SelectionJSN: selection, isSelection: false)
        selections.append(selectionobj)
       
        
        selection = JSON()
        selection["categoryId"].stringValue = currentIndexName
        selection["id"] = "2"
        selection["title"] = "Move the buiult-in over from under thehalf to the cobinet and get 3 ladders under the hall window."
        selection["isListing"] = "false"
        selection["isSelected"] = "false"
        let selectionobj1 = Selection.init(SelectionJSN: selection, isSelection: false)
        selections.append(selectionobj1)
        
        selection = JSON()
        selection["categoryId"].stringValue = currentIndexName
        selection["id"] = "3"
        selection["title"] = "Ekectrolux strainless steel microscope 6900"
        selection["isListing"] = "false"
        selection["isSelected"] = "false"
        let selectionobj2 = Selection.init(SelectionJSN: selection, isSelection: false)
        selections.append(selectionobj2)
        selectionList.append(selections)
        selections.removeAll()
        
        selection = JSON()
        selection["categoryId"].stringValue = currentIndexName
        selection["id"] = "4"
        selection["title"] = "Remove the freezer and replace wit a combination / freezer and a cabnet of 6900"
        selection["isListing"] = "false"
        selection["isSelected"] = "false"
        let selectionobj3 = Selection.init(SelectionJSN: selection, isSelection: false)
        selections.append(selectionobj3)
        
        
        selection = JSON()
        selection["categoryId"].stringValue = currentIndexName
        selection["id"] = "5"
        selection["title"] = "Move the buiult-in over from under thehalf to the cobinet and get 3 ladders under the hall window."
        selection["isListing"] = "false"
        selection["isSelected"] = "false"
        let selectionobj4 = Selection.init(SelectionJSN: selection, isSelection: false)
        selections.append(selectionobj4)
        
        selection = JSON()
        selection["categoryId"].stringValue = currentIndexName
        selection["id"] = "6"
        selection["title"] = "Ekectrolux strainless steel microscope 6900"
        selection["isListing"] = "false"
        selection["isSelected"] = "false"
        let selectionobj5 = Selection.init(SelectionJSN: selection, isSelection: false)
        selections.append(selectionobj5)
        
        
        selectionList.append(selections)
        selections.removeAll()
        
        selection = JSON()
        selection["categoryId"].stringValue = currentIndexName
        selection["id"] = "7"
        selection["title"] = "Remove the freezer and replace wit a combination / freezer and a cabnet of 6900"
        selection["isListing"] = "false"
        selection["isSelected"] = "false"
        let selectionobj6 = Selection.init(SelectionJSN: selection, isSelection: false)
        selections.append(selectionobj6)
        
        
        selection = JSON()
        selection["categoryId"].stringValue = currentIndexName
        selection["id"] = "8"
        selection["title"] = "Move the buiult-in over from under thehalf to the cobinet and get 3 ladders under the hall window."
        selection["isListing"] = "false"
        selection["isSelected"] = "false"
        let selectionobj7 = Selection.init(SelectionJSN: selection, isSelection: false)
        selections.append(selectionobj7)
        
        selection = JSON()
        selection["categoryId"].stringValue = currentIndexName
        selection["id"] = "6"
        selection["title"] = "Ekectrolux strainless steel microscope 6900"
        selection["isListing"] = "false"
        selection["isSelected"] = "false"
        let selectionobj8 = Selection.init(SelectionJSN: selection, isSelection: false)
        selections.append(selectionobj8)
        
        
        selectionList.append(selections)
        print("selectionList-->",selectionList)
       
        tblViewSelectingList.reloadData()
    }
    //MARK:- Buttom Action Method
   
    @objc func checkUncheckList(_sender:AnyObject)
    {        
        let buttonPosition = (_sender as AnyObject).convert(CGPoint.zero, to: self.tblViewSelectingList)
        let indexPath = self.tblViewSelectingList.indexPathForRow(at: buttonPosition)
        if indexPath != nil
        {
            var selection = selectionList[indexPath!.section][indexPath!.row].tempSelectionJSN!
            var isSelected = selection["isSelected"].stringValue
            isSelected = isSelected == "true" ? "false" : "true"
            selection["isSelected"].stringValue = isSelected
            selectionList[indexPath!.section][indexPath!.row].tempSelectionJSN! = selection
            //tblViewSelectingList.reloadRows(at: [indexPath], with: .none)
            print("selection",selection)
            var isAnyItemSelected = Bool()
            for (_,item) in selectionList.enumerated()
            {
                for i in 0...item.count-1{
                    var selection = item[i].tempSelectionJSN!
                    let isSelected = selection["isSelected"].boolValue
                    if isSelected{
                        isAnyItemSelected = true
                        break
                    }
                }
            }
            if isAnyItemSelected{
                self.tblViewSelectingList.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 40, right: 0)
                nextButtonDelegate?.NextButtonVisibleNonListAction(dic: selectionList)
            }else{
                self.tblViewSelectingList.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
                nextButtonDelegate?.HideNextButton()
            }
            tblViewSelectingList.reloadData()
        }
    }
    
    
    
}
//MARK:- UITableView delegate datasource methods
extension OtherSelectingListVC : UITableViewDelegate,UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return selectionList.count
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0{
            return 1
        }
        return 15
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return selectionList[section].count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OtherPropertySelectiingCell", for: indexPath as IndexPath) as! OtherPropertySelectiingCell
        let selection = selectionList[indexPath.section][indexPath.row].tempSelectionJSN!
       // let selection = arrCategoryList[indexPath.row]
        cell.setData(data: selection)
        cell.btnChekUncheck.addTarget(self, action: #selector(checkUncheckList(_sender:)), for: .touchUpInside)
        let last = selectionList[indexPath.section]
        if indexPath.row == last.count-1{
            cell.lblBottom.backgroundColor = UIColor.clear
        }else{
            cell.lblBottom.backgroundColor = UIColor.appThemeLightGrayColor
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var selection = selectionList[indexPath.section][indexPath.row].tempSelectionJSN!
        var isSelected = selection["isSelected"].stringValue
        isSelected = isSelected == "true" ? "false" : "true"
        selection["isSelected"].stringValue = isSelected
        selectionList[indexPath.section][indexPath.row].tempSelectionJSN! = selection
        //tblViewSelectingList.reloadRows(at: [indexPath], with: .none)
        print("selection",selection)
        var isAnyItemSelected = Bool()
        for (_,item) in selectionList.enumerated()
        {
            for i in 0...item.count-1{
                var selection = item[i].tempSelectionJSN!
                let isSelected = selection["isSelected"].boolValue
                if isSelected{
                    isAnyItemSelected = true
                    break
                }
            }
        }
        if isAnyItemSelected{
            self.tblViewSelectingList.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 40, right: 0)
            nextButtonDelegate?.NextButtonVisibleNonListAction(dic: selectionList)
        }else{
            self.tblViewSelectingList.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            nextButtonDelegate?.HideNextButton()
        }
        tblViewSelectingList.reloadData()
    }
    
}


//MARK:- Service

extension OtherSelectingListVC
{
    func getCategoryInfo()
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(kBasicURL)\(kCategoryInfoData)"
            
            print("URL: \(url)")
            
            let param =  ["lang":strLang,
                          "user_id":getUserDetail("user_id"),
                          "access_token":getUserDetail("access_token"),
                          "category_id":dictData["category_id"].stringValue
            ]
            
            print("Param : \(param)")
            
            showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.stopLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == strSuccessResponse
                    {
                        self.arrCategoryList = []
                        self.arrCategoryList = json["data"].arrayValue
                        
                        for i in 0..<self.arrCategoryList.count
                        {
                            var dict = self.arrCategoryList[i]
                            dict["is_selected"] = "false"
                            self.arrCategoryList[i] = dict
                        }
                    }
                    else
                    {
                        self.arrCategoryList = []
                        self.strMsg = json["msg"].stringValue
                        makeToast(message: json["msg"].stringValue)
                    }
                    self.tblViewSelectingList.reloadData()
                    
                }
                else
                {
                    makeToast(message: getCommonString(key: "Something_went_wrong_key"))
                }
            }
            
        }
        else
        {
            makeToast(message: getCommonString(key: "No_internet_connection_key"))
        }
    }
    
}


