//
//  StartUpVC.swift
//  BJInvest
//
//  Created by Abhay on 02/01/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import UIKit

class StartUpVC: UIViewController {
    //MARK:- Outlets
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblAssignDate: UILabel!
    @IBOutlet weak var lblLastUpdated: UILabel!
    @IBOutlet weak var lblStatupTitle: UILabel!
    @IBOutlet weak var btnStartChange: CustomButton!
    
     //MARK:- Declaration
    
    
     //MARK:- View lifecycle
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(userLogout), name: NSNotification.Name(rawValue: "logoutAPI"), object: nil)
        // Do any additional setup after loading the view.
        decoreUI()
    }
    

     //MARK:- Custo0m Methods
    
    func decoreUI()
    {
        setupNavigationBar()
        lblUserName.textColor = UIColor.black
        lblUserName.font = themeFont(size: 18, fontname: .semibold)
        lblUserName.text = getUserDetail("full_name")
        
        let assignDate = getCommonString(key: "Assign_date_key")
        let LastUpdated = getCommonString(key: "Last_updated_key")
        lblAssignDate.attributedText = "\(assignDate) \(getUserDetail("assign_date"))".attributedStringForPartiallyColoredText("\(assignDate)", with: UIColor.appThemeLightGrayColor)
        lblLastUpdated.attributedText = "\(LastUpdated) \(getUserDetail("last_updated_date"))".attributedStringForPartiallyColoredText("\(LastUpdated)", with: UIColor.appThemeLightGrayColor)
         lblAssignDate.font = themeFont(size: 16, fontname: .semibold)
         lblLastUpdated.font = themeFont(size: 16, fontname: .semibold)
        let starupTitle = "\(getCommonString(key: "select_your_key"))\n\(getCommonString(key: "Preferences_choice_key"))\n\(getCommonString(key: "For_your_key"))\n\(getCommonString(key: "Dream_house_key"))"
        lblStatupTitle.font = themeFont(size: 20, fontname: .semibold)
        lblStatupTitle.text = starupTitle
        btnStartChange.setUpThemeButtonUI(fontColor: UIColor.white, size: 20, type: .regular, title: getCommonString(key: "Start_key").uppercased(), bgcolor: UIColor.clear)
        
    }
    func setupNavigationBar()
    {
        self.edgesForExtendedLayout = UIRectEdge.init(rawValue: 0)
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        
        
        
        /* let leftImage  = UIImage(named: "ic_back_arrow_white_header")!.withRenderingMode(.alwaysOriginal)
         var leftButton = UIButton(frame: CGRect(x:0,y: 0,width: 20,height: 44))
         leftButton = UIButton(type: .custom)
         leftButton.tintColor = .white
         leftButton.setBackgroundImage(leftImage, for: .normal)
         leftButton.addTarget(self, action: #selector(backButtonAction), for:.touchUpInside)
         
         let menuButton = UIBarButtonItem(customView: leftButton)
         self.navigationItem.leftBarButtonItem = menuButton
         self.navigationItem.hidesBackButton = false*/
        let Image  = UIImage(named: "ic_logo_header")
        
        let rightButton = UIBarButtonItem(image: UIImage(named: "ic_more_header"), style: .plain, target: self, action: #selector(manageMenubar))
        rightButton.tintColor = UIColor.lightGray
        self.navigationItem.rightBarButtonItem = rightButton
        
        let HeaderView = UILabel.init(frame: CGRect(x: 0, y: 0, width: 40, height: 19))
        HeaderView.isUserInteractionEnabled = false
        HeaderView.text = ""
        HeaderView.font = themeFont(size: 18, fontname: .semibold)
        HeaderView.textColor = UIColor.white
        self.navigationItem.titleView = UIImageView(image: Image)
        
        self.navigationItem.hidesBackButton = true
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.layer.masksToBounds = false
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.lightGray.cgColor
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.55
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        self.navigationController?.navigationBar.layer.shadowRadius = 2
        self.view.backgroundColor = UIColor.appThemeBackgroundColor
    }
     //MARK:- Button Actions
    @objc func manageMenubar()
    {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "PopupVC") as! PopupVC
        obj.popupDelegate = self
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .crossDissolve
        self.present(obj, animated: false, completion: nil)
    
    }
    
    @IBAction func btnStartChangeAction(_ sender: UIButton) {
        let MainServicesListVC = self.storyboard?.instantiateViewController(withIdentifier: "MainServicesListVC") as! MainServicesListVC
        self.navigationController?.pushViewController(MainServicesListVC, animated: true)
    }
    
}
//MARK: - Popup Delegate

extension StartUpVC : popupCommonDelegate
{
    func NotificationAction() {
        let NotificationVC = self.storyboard?.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
        self.navigationController?.pushViewController(NotificationVC, animated: true)
    }
    
    
    func LogoutAction() {
        self.perform(#selector(logoutAlert), with: nil, afterDelay: 0.3)
       // logoutAlert()
    }
    
    @objc func logoutAlert()
    {
        let alertController = UIAlertController(title: "", message: getCommonString(key: "Are_you_sure_want_to_logout?_key"), preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: getCommonString(key: "Yes_key"), style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            
            self.logoutAPICalling()
            
        }
        let cancelAction = UIAlertAction(title: getCommonString(key: "No_key"), style: UIAlertAction.Style.cancel) { (result : UIAlertAction) -> Void in
            print("Cancel")
        }
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    func logoutAPICalling()
    {
        self.userLogout()
    }
    
    
}
