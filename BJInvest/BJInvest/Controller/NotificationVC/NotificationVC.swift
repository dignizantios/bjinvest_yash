//
//  NotificationVC.swift
//  BJInvest
//
//  Created by Haresh Bhai on 08/01/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import AlamofireSwiftyJSON

class NotificationVC: UIViewController {

    //MARK:- Outlets
    
    @IBOutlet weak var tblViewNotification: UITableView!
    
    //MARK:- Declaration
    var arrNotificationList:[JSON] = []
    var strMsg = String()
    
    
    //MARK:- View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let Notification = getCommonString(key: "Notification_key").uppercased()
        setupNavigationBarWithBackButton(titleText:Notification)
        self.view.backgroundColor = UIColor.appThemeBackgroundColor
        tblViewNotification.delegate = self
        tblViewNotification.dataSource = self
        getNotificationList()
    }
    func decoreUI()
    {
        self.navigationItem.hidesBackButton = true
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.layer.masksToBounds = false
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.lightGray.cgColor
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.55
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        self.navigationController?.navigationBar.layer.shadowRadius = 2
        tblViewNotification.tableFooterView = UIView()
    }

    //MARK:- Custom Methods

}
 //MARK:- UITableView Datasource delegate methods

extension NotificationVC : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrNotificationList.count == 0
        {
            let lbl = UILabel()
            lbl.text = strMsg
            lbl.font = themeFont(size: 20, fontname: .regular)
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = UIColor.black
            lbl.center = tableView.center
            tableView.backgroundView = lbl
            return 0
        }
        tableView.backgroundView = nil
        return arrNotificationList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath as IndexPath) as! NotificationCell
        cell.setData(data: arrNotificationList[indexPath.row])
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
}

//MARK:- Service

extension NotificationVC
{
    func getNotificationList()
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(kBasicURL)\(kCategoryList)"
            
            print("URL: \(url)")
            
            let param =  ["lang":strLang,
                          "user_id":getUserDetail("user_id"),
                          "access_token":getUserDetail("access_token")
            ]
            
            print("Param : \(param)")
            
            showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.stopLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == strSuccessResponse
                    {
                        
                        self.arrNotificationList = []
                        self.arrNotificationList = json["data"].arrayValue
                      
                    }
                    else
                    {
                        self.arrNotificationList = []
                        self.strMsg = json["msg"].stringValue
                        makeToast(message: json["msg"].stringValue)
                    }
                    self.tblViewNotification.reloadData()
                    
                }
                else
                {
                    makeToast(message: getCommonString(key: "Something_went_wrong_key"))
                }
            }
            
        }
        else
        {
            makeToast(message: getCommonString(key: "No_internet_connection_key"))
        }
    }
    
}

