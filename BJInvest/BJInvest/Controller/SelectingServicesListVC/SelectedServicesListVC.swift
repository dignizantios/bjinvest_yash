//
//  SelectedServicesListVC.swift
//  BJInvest
//
//  Created by Haresh Bhai on 08/01/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import UIKit

class SelectedServicesListVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblAssignDate: UILabel!
    @IBOutlet weak var lblLastUpdated: UILabel!
    @IBOutlet weak var lblStatupTitle: UILabel!
    @IBOutlet weak var btnChange: CustomButton!
    @IBOutlet weak var tblViewSummery: UITableView!
    
    //MARK:- Declaration
    var NoOfCnt = Int()
    
    //MARK:- View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
         self.view.backgroundColor = UIColor.appThemeBackgroundColor
        decoreUI()
    }
    

    //MARK:- Custo0m Methods
    
    func decoreUI()
    {
        setupNavigationBar()
        lblUserName.textColor = UIColor.black
        lblUserName.font = themeFont(size: 18, fontname: .semibold)
        lblUserName.text = "Villa Nybrostrand"
        
        let assignDate = getCommonString(key: "Assign_date_key")
        let LastUpdated = getCommonString(key: "Last_updated_key")
        lblAssignDate.attributedText = "\(assignDate) 25-01-2019".attributedStringForPartiallyColoredText("\(assignDate)", with: UIColor.appThemeLightGrayColor)
        lblLastUpdated.attributedText = "\(LastUpdated) 25-01-2019".attributedStringForPartiallyColoredText("\(LastUpdated)", with: UIColor.appThemeLightGrayColor)
        lblAssignDate.font = themeFont(size: 16, fontname: .semibold)
        lblLastUpdated.font = themeFont(size: 16, fontname: .semibold)
        let starupTitle = "\(getCommonString(key: "Want_to_change_choise_key"))\n\(getCommonString(key: "Choise_key"))"
        lblStatupTitle.font = themeFont(size: 20, fontname: .semibold)
        lblStatupTitle.text = starupTitle
        btnChange.setUpThemeButtonUI(fontColor: UIColor.white, size: 20, type: .regular, title: getCommonString(key: "Change_key").uppercased(), bgcolor: UIColor.clear)
        self.tblViewSummery.dataSource = self
        self.tblViewSummery.delegate = self
       // self.tblViewSummery.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0)
        if summuryCheckedList.count > 0{
            NoOfCnt = summuryCheckedList.count+summurySelectionList.count
        }else{
            NoOfCnt = summurySelectionList.count
        }
    }
    func setupNavigationBar()
    {
        self.edgesForExtendedLayout = UIRectEdge.init(rawValue: 0)
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        
        let Image  = UIImage(named: "ic_logo_header")
        
        let rightButton = UIBarButtonItem(image: UIImage(named: "ic_more_header"), style: .plain, target: self, action: #selector(manageMenubar))
        rightButton.tintColor = UIColor.lightGray
        self.navigationItem.rightBarButtonItem = rightButton
        
        let HeaderView = UILabel.init(frame: CGRect(x: 0, y: 0, width: 40, height: 19))
        HeaderView.isUserInteractionEnabled = false
        HeaderView.text = ""
        HeaderView.font = themeFont(size: 18, fontname: .semibold)
        HeaderView.textColor = UIColor.white
        self.navigationItem.titleView = UIImageView(image: Image)
        
        self.navigationItem.hidesBackButton = true
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.layer.masksToBounds = false
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.lightGray.cgColor
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.55
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        self.navigationController?.navigationBar.layer.shadowRadius = 2
        
    }
    //MARK:- Button Actions
    @objc func manageMenubar()
    {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "PopupVC") as! PopupVC
        obj.popupDelegate = self
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .crossDissolve
        self.present(obj, animated: false, completion: nil)
        
    }
    
    @IBAction func btnChangeAction(_ sender: UIButton) {
        let MainServicesListVC = self.storyboard?.instantiateViewController(withIdentifier: "MainServicesListVC") as! MainServicesListVC
        self.navigationController?.pushViewController(MainServicesListVC, animated: true)
    }

}
//MARK: - Popup Delegate

extension SelectedServicesListVC : popupCommonDelegate

{
    func NotificationAction() {
        
        let NotificationVC = self.storyboard?.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
        self.navigationController?.pushViewController(NotificationVC, animated: true)
    }
    
    
    
    func LogoutAction() {
        
        self.perform(#selector(logoutAlert), with: nil, afterDelay: 0.5)
        
    }
    
    @objc func logoutAlert()
    {
        let alertController = UIAlertController(title: "", message: getCommonString(key: "Are_you_sure_want_to_logout?_key"), preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: getCommonString(key: "Yes_key"), style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            
            self.logoutAPICalling()
            
        }
        let cancelAction = UIAlertAction(title: getCommonString(key: "No_key"), style: UIAlertAction.Style.cancel) { (result : UIAlertAction) -> Void in
            print("Cancel")
        }
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    func logoutAPICalling()
    {
        
        //let vc  = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "LoginVc") as! LoginVc
        
        //let rearNavigation = UINavigationController(rootViewController: vc)
        //AppDelegate.shared.window?.rootViewController = rearNavigation
        
    }
    
    
}
extension SelectedServicesListVC : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if summuryCheckedList.count > 0{
            return summuryCheckedList.count+summurySelectionList.count
        }else{
            return summurySelectionList.count
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if summuryCheckedList.count < 0{
            let isListing = summurySelectionList[indexPath.row]["isListing"].boolValue
            if isListing{
                let cell = tableView.dequeueReusableCell(withIdentifier: "SummeryEditPropertyCell", for: indexPath as IndexPath) as! SummeryEditPropertyCell
                cell.setData(data: summurySelectionList[indexPath.row])
                cell.btnCheckmark.isHidden = true
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "SummeryOtherPropertyCell", for: indexPath as IndexPath) as! SummeryOtherPropertyCell
                //cell.setData(data: selection)
                return cell
            }
        }else{
            
            if indexPath.row <= summurySelectionList.count-1{
                let isListing = summurySelectionList[indexPath.row]["isListing"].boolValue
                if isListing{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "SummeryEditPropertyCell", for: indexPath as IndexPath) as! SummeryEditPropertyCell
                    cell.setData(data: summurySelectionList[indexPath.row])
                    cell.btnCheckmark.isHidden = true
                    return cell
                }else{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "SummeryOtherPropertyCell", for: indexPath as IndexPath) as! SummeryOtherPropertyCell
                    //cell.setData(data: selection)
                    return cell
                }
            }
            else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "SummeryOtherPropertyCell", for: indexPath as IndexPath) as! SummeryOtherPropertyCell
                //cell.setData(data: selection)
                var inx = summurySelectionList.count
                inx = indexPath.row - inx
                cell.lblTitle.text = summuryCheckedList[inx]["title"].stringValue
                if indexPath.row == NoOfCnt-1{
                    cell.lblBottomSeperator.isHidden = true
                }else{
                    cell.lblBottomSeperator.isHidden = false
                }
                return cell
                
                
            }
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
}

