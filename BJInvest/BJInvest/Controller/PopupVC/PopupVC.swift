//
//  PopupVC.swift
//  BJInvest
//
//  Created by Haresh Bhai on 02/01/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit

protocol popupCommonDelegate  {
    
    func NotificationAction()
    
    func LogoutAction()
    
}
class PopupVC: UIViewController,UIGestureRecognizerDelegate {

    //MARK: - Outlet
    
    @IBOutlet weak var vwOption: UIView!
    
    @IBOutlet weak var vwNotification: UIView!
    @IBOutlet weak var lblNotification: UILabel!
    @IBOutlet weak var lblLogout: UILabel!
    
    @IBOutlet weak var vwLogout: UIView!
    @IBOutlet weak var vwHeight: NSLayoutConstraint!
    //MARK: - Variable
    
    var popupDelegate : popupCommonDelegate?
    
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupUI()
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        
        showAnimate()
    }
    
    override func viewDidDisappear(_ animated: Bool)
    {
        super.viewDidDisappear(animated)
        removeAnimate()
    }
    
    func setupUI()
    {
        self.vwHeight.constant = 0
        let tap = UITapGestureRecognizer(target: self, action: #selector(TappedGesture))
        tap.delegate = self
        self.view.addGestureRecognizer(tap)
        
        
        [lblNotification,lblLogout].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeDarkGrayColor
            lbl?.font = themeFont(size: 15, fontname: .regular)
        }
        
        lblNotification.text = getCommonString(key: "Notification_key")
        lblLogout.text = getCommonString(key: "Logout_key")
        
        
    }
    
    @IBAction func btnNotoificationAction(_ sender: UIButton) {
        removeAnimate()
        
        popupDelegate?.NotificationAction()
    }
    
    
    
}

//MARK: - Animation
extension PopupVC
{
    
    func showAnimate()
    {
        
        self.vwOption.isHidden = false
        self.vwHeight.constant = 90
        
        UIView.animate(withDuration: 0.3, animations: {
            //  self.view.alpha = 1.0
            
            self.view.layoutIfNeeded()
            
        })
    }
    
    func removeAnimate()
    {
        
        self.vwHeight.constant = 0.0
        
        self.vwLogout.isHidden = true
        self.vwNotification.isHidden = true
        
        UIView.animate(withDuration: 0.3, animations: {
            //  self.view.alpha = 1.0
            
            self.view.layoutIfNeeded()
            self.perform(#selector(self.dismisscontroller), with: nil, afterDelay: 0.3)
            
        })
    }
    
    
    @objc func dismisscontroller()
    {
        dismiss(animated: true, completion: nil)
    }
    
}


//MARK: - IBAction

extension PopupVC
{
    
    @objc func TappedGesture()
    {
        removeAnimate()
    }
    
    @IBAction func btnNotoficationTapped(_ sender: UIButton) {
        
        removeAnimate()
        
        popupDelegate?.NotificationAction()
        
    }
    
    @IBAction func btnLogoutTapped(_ sender: UIButton) {
        
        removeAnimate()
        
        popupDelegate?.LogoutAction()
        
    }
    
}

