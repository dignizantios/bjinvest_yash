//
//  SummeryVC.swift
//  BJInvest
//
//  Created by Haresh Bhai on 04/01/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import UIKit
import SwiftyJSON

class SummeryVC: UIViewController {

    //MARK:- Outlets
    
    @IBOutlet weak var tblViewSummery: UITableView!
    @IBOutlet weak var lnlTotalItem: UILabel!
    @IBOutlet weak var lblNoOfItem: UILabel!
    @IBOutlet weak var lblTotal: UILabel!
    
    @IBOutlet weak var lblTotalPrice: UILabel!
    @IBOutlet weak var btnSend: CustomButton!
    
    
    //MARK:- Declaration
    var summurySelectedList:[JSON] = []
    var totalPrice = Int()
    var NoOfCnt = Int()
    //MARK:- View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
         self.view.backgroundColor = UIColor.appThemeBackgroundColor
        let Summery = getCommonString(key: "Summery_key")
        setupNavigationBarWithBackButton(titleText:Summery)
        summurySelectedList = summurySelectionList
        self.view.backgroundColor = UIColor.appThemeBackgroundColor
        tblViewSummery.delegate = self
        tblViewSummery.dataSource = self
        decoreUI()
    }
    

   // MARK: - Custom Methods
    func decoreUI()
    {
        self.navigationItem.hidesBackButton = true
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.layer.masksToBounds = false
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.lightGray.cgColor
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.55
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        self.navigationController?.navigationBar.layer.shadowRadius = 2
        self.tblViewSummery.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 20, right: 0)
        
        if summuryCheckedList.count > 0{
            NoOfCnt = summuryCheckedList.count+summurySelectedList.count
        }else{
            NoOfCnt = summurySelectedList.count
        }
        lnlTotalItem.text = getCommonString(key: "Total_item_key")
        lblNoOfItem.text = "\(NoOfCnt)"
        lblTotal.text = getCommonString(key: "Total_key")
        for (_,item) in summurySelectedList.enumerated(){
            let isListing = item["isListing"].boolValue
            if isListing{
                print("price,,-",item["price"].stringValue)
                let price = item["price"].intValue
                print("price-->",price)
                totalPrice += price
                 print("totalPrice-->",totalPrice)
            }
            
        }
        lblTotalPrice.text = "\(getCommonString(key: "Kr_key")) "+"\(totalPrice)"
        
    }
     //MARK:- Buttom Action Method
    
    @IBAction func btnSendAction(_ sender: UIButton) {
        let SelectedServicesListVC = self.storyboard?.instantiateViewController(withIdentifier: "SelectedServicesListVC") as! SelectedServicesListVC
        self.navigationController?.pushViewController(SelectedServicesListVC, animated: true)
    }
   
    
    @objc func btnChecklistEdit(_ sender:UIButton)
    {
        let title = summurySelectedList[sender.tag]["categoryId"].stringValue
        if let vc = navigationController!.viewControllers.filter({ $0 is MainServicesListVC }).first as? MainServicesListVC{
            vc.isEdited = true
            vc.editSelection = title
            navigationController!.popToViewController(vc, animated: true)
        }
        
    }
    
    @objc func btnNotChecklistEdit(_ sender:UIButton)
    {
        let title = summuryCheckedList[sender.tag]["categoryId"].stringValue
        if let vc = navigationController!.viewControllers.filter({ $0 is MainServicesListVC }).first as? MainServicesListVC{
            vc.isEdited = true
            vc.editSelection = title
            navigationController!.popToViewController(vc, animated: true)
        }
    }
    
}
//MARK:- UITableView delegate datasource methods
extension SummeryVC : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if summuryCheckedList.count > 0{
            return summuryCheckedList.count+summurySelectedList.count+1
        }else{
            return summurySelectedList.count
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if summuryCheckedList.count < 0{
            let isListing = summurySelectedList[indexPath.row]["isListing"].boolValue
            if isListing{
                let cell = tableView.dequeueReusableCell(withIdentifier: "SummeryEditPropertyCell", for: indexPath as IndexPath) as! SummeryEditPropertyCell
                cell.setData(data: summurySelectedList[indexPath.row])
                cell.btnCheckmark.tag = indexPath.row
                 cell.btnCheckmark.addTarget(self, action: #selector(btnChecklistEdit(_:)), for: .touchUpInside)
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "SummeryOtherPropertyCell", for: indexPath as IndexPath) as! SummeryOtherPropertyCell
                //cell.setData(data: selection)
                return cell
            }
        }else{
           
            if indexPath.row <= summurySelectedList.count-1{
                let isListing = summurySelectedList[indexPath.row]["isListing"].boolValue
                if isListing{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "SummeryEditPropertyCell", for: indexPath as IndexPath) as! SummeryEditPropertyCell
                    cell.setData(data: summurySelectedList[indexPath.row])
                    cell.btnCheckmark.tag = indexPath.row
                     cell.btnCheckmark.addTarget(self, action: #selector(btnChecklistEdit(_:)), for: .touchUpInside)
                    return cell
                }else{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "SummeryOtherPropertyCell", for: indexPath as IndexPath) as! SummeryOtherPropertyCell
                    //cell.setData(data: selection)
                    return cell
                }
            }
            else if indexPath.row == NoOfCnt{
                let cell = tableView.dequeueReusableCell(withIdentifier: "SummeryOtherPropertyEditCell", for: indexPath as IndexPath) as! SummeryOtherPropertyEditCell
                cell.btnEdit.tag = indexPath.row
                 cell.btnEdit.addTarget(self, action: #selector(btnNotChecklistEdit(_:)), for: .touchUpInside)
                return cell
            }
            else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "SummeryOtherPropertyCell", for: indexPath as IndexPath) as! SummeryOtherPropertyCell
                //cell.setData(data: selection)
                var inx = summurySelectedList.count
                inx = indexPath.row - inx
                cell.lblTitle.text = summuryCheckedList[inx]["title"].stringValue
                if indexPath.row == NoOfCnt-1{
                    cell.lblBottomSeperator.isHidden = true
                }else{
                    cell.lblBottomSeperator.isHidden = false
                }
                return cell
                
                
            }
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
}

