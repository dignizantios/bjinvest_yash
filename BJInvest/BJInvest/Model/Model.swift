
//
//  Model.swift
//  PizzaApp
//
//  Created by Jaydeep on 02/01/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//
//Last push

import Foundation
import SwiftyJSON

class User: NSObject
{
    //MARK:- Store Detail Variable Declarattion
    
    var strUserName : String = ""
    var strPassword : String = ""
    
    //MARK:- Return Validation message
    
    var strValidationMessage : String = ""
    
    //MARK:- Validation Function
    
    func isLogin() -> Bool {
        
        if (strUserName.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = getValidationString(key: "Please_enter_email_address_or_username_key")
            return false
        }
        else if (strPassword.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = getValidationString(key: "Please_enter_your_password_key")
            return false
        }
        return true
    }
   
}

