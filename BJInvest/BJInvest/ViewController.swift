//
//  ViewController.swift
//  BJInvest
//
//  Created by Abhay on 01/01/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import AlamofireSwiftyJSON

class ViewController: UIViewController {
    
    //MARK:- Variable Delcration
    
    @IBOutlet weak var lblLoginTitle: UILabel!
    @IBOutlet weak var txtUsername: CustomTextField!
    @IBOutlet weak var txtPassword: CustomTextField!
    @IBOutlet weak var btnForgotPasswordOutlet: UIButton!
    @IBOutlet weak var btnLoginOutlet: CustomButton!
    
    
    //MARK:- Outlet Zone
    
    //MARK:- ViewLife cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if getUserDetail("user_id") != ""
        {
            let StartUpVC = self.storyboard?.instantiateViewController(withIdentifier: "StartUpVC") as! StartUpVC
            self.navigationController?.pushViewController(StartUpVC, animated: true)
        }
        setupUI()
        //printFonts()
    }

}

//MARK:- Setup UI

extension ViewController
{
    func setupUI()
    {
        lblLoginTitle.textColor = UIColor.white
        lblLoginTitle.font = themeFont(size: 17, fontname: .regular)
        lblLoginTitle.text = getCommonString(key: "Login_key").uppercased()      
        
        
        self.btnForgotPasswordOutlet.setUpThemeButtonUI(fontColor: UIColor.white, size: 15, type: .regular, title: "\(getCommonString(key: "Forgot_password_key").capitalized)?", bgcolor: UIColor.clear)
        
        txtUsername.placeholder = getCommonString(key: "Username_key")
        txtPassword.placeholder = getCommonString(key: "Password_key")
        
        [txtUsername,txtPassword].forEach { (txtFiled) in
            txtFiled?.delegate = self
            txtFiled?.font = themeFont(size: 17, fontname: .regular)
            txtFiled?.textColor = UIColor.white
            txtFiled?.placeHolderColor = UIColor.white
            //txtFiled?.text = ""
        }
        
        btnLoginOutlet.setUpThemeButtonUI(fontColor: UIColor.appThemeLightGrayColor, size: 18, type: .regular, title: getCommonString(key: "Login_key").uppercased(), bgcolor: UIColor.white)
        
    }
}

//MARK:- Textfield Delegate

extension ViewController:UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
}

//MARK:- Action Zone

extension ViewController
{
    @IBAction func btnForgotPasswordAction(_ sender:UIButton)
    {
        
    }
    
    @IBAction func btnLoginAction(_ sender:UIButton)
    {
        objUser = User()
        objUser.strUserName = self.txtUsername.text ?? ""
        objUser.strPassword = self.txtPassword.text ?? ""
        
        if objUser.isLogin()
        {
            userLogin()
        }
        else
        {
            makeToast(message: objUser.strValidationMessage)
        }
    }
}

//MARK:- Service

extension ViewController
{
    func userLogin()
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(kBasicURL)\(kLogin)"
            
            print("URL: \(url)")
            
            let param =  ["lang":strLang,
                          "username":objUser.strUserName,
                          "password":objUser.strPassword,
                          "device_token":"",
                          "register_id":"",
                          "device_type":strDeviceType,
                          "timezone":localTimeZoneName]
            
            
            print("Param : \(param)")
            
            showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.stopLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == strSuccessResponse
                    {
                        let data = json["data"]
                        guard let rowdata = try? data.rawData() else {return}
                        Defaults.setValue(rowdata, forKey: "userDetail")
                        Defaults.synchronize()
                        
                        let StartUpVC = self.storyboard?.instantiateViewController(withIdentifier: "StartUpVC") as! StartUpVC
                        self.navigationController?.pushViewController(StartUpVC, animated: true)
                        
                    }
                    else
                    {
                        makeToast(message: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    makeToast(message: getCommonString(key: "Something_went_wrong_key"))
                }
            }
            
        }
        else
        {
            makeToast(message: getCommonString(key: "No_internet_connection_key"))
        }
    }
    
}

