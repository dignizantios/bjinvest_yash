//
//  Service.swift
//  Hand2Home
//
//  Created by YASH on 02/02/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON

struct CommonService
{
    func Service(url: String,param: [String:String],completion:@escaping(Result<JSON>)-> ())
    {
        print("url - ",url)
        
        Alamofire.request(url, method: .post, parameters: param, encoding: URLEncoding.default, headers: nil).authenticate(user: basic_username, password:basic_password).responseSwiftyJSON(completionHandler:
            {
               
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    print("StatusCode : \(statusCode)")
                    if(statusCode == 500)
                    {
                        
                    }else if(statusCode != nil)
                    {
                        completion($0.result)
                    }
                    else
                    {
                        makeToast(message: getCommonString(key: "Something_went_wrong_key"))
                        completion($0.result)
                    }
                }else
                {
                    makeToast(message: getCommonString(key: "No_internet_connection_key"))
                    completion($0.result)
                }
        })
    }    
    
    
}
