//
//  SummeryOtherPropertyEditCell.swift
//  BJInvest
//
//  Created by Haresh Bhai on 04/01/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import UIKit

class SummeryOtherPropertyEditCell: UITableViewCell {

    @IBOutlet weak var vwBackView: CustomView!
    
    @IBOutlet weak var btnEdit: UIButton!
    
    var btnAction: (()-> Void)!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func btnEdit(_ sender: UIButton) {
       btnAction()
    }


}
