//
//  SummeryOtherPropertyCell.swift
//  BJInvest
//
//  Created by Haresh Bhai on 04/01/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import UIKit
import SwiftyJSON

class SummeryOtherPropertyCell: UITableViewCell {
    
    @IBOutlet weak var vwBackView: CustomView!
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var lblBottomSeperator: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
