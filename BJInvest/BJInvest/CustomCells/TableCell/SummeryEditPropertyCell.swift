//
//  SummeryEditPropertyCell.swift
//  BJInvest
//
//  Created by Haresh Bhai on 04/01/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import UIKit
import SwiftyJSON

class SummeryEditPropertyCell: UITableViewCell {

    @IBOutlet weak var imgSelection: CustomImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var btnCheckmark: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblTitle.textColor = UIColor.black
        lblTitle.font = themeFont(size: 18, fontname: .semibold)
        
        lblPrice.textColor = UIColor.appThemeLightGrayColor
        lblPrice.font = themeFont(size: 22, fontname: .semibold)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData (data:JSON)
    {
        imgSelection.image = UIImage(named: "\(data["img"].stringValue)")
        lblTitle.text = data["title"].stringValue
        let kr = getCommonString(key: "Kr_key")
        lblPrice.text = "\(kr) \(data["price"].stringValue)"
        let isSelected = data["isSelected"].boolValue
        if isSelected{
            btnCheckmark.isHidden = false
        }else{
            btnCheckmark.isHidden = true
        }
        
    }

}
