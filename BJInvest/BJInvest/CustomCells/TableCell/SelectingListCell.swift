//
//  SelectingListCell.swift
//  BJInvest
//
//  Created by Haresh Bhai on 03/01/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import UIKit
import SwiftyJSON
import SDWebImage

class SelectingListCell: UITableViewCell {

    @IBOutlet weak var imgSelection: CustomImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var imgCheckmark: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        lblTitle.textColor = UIColor.black
        lblTitle.font = themeFont(size: 18, fontname: .semibold)
        
        lblPrice.textColor = UIColor.appThemeLightGrayColor
        lblPrice.font = themeFont(size: 22, fontname: .semibold)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setData (data:JSON)
    {
        imgSelection.sd_setImage(with: data["image"].url, placeholderImage: UIImage.init(named: "image_placeholder_main_list"), options: .lowPriority, completed: nil)
        lblTitle.text = data["property_title"].stringValue
        let kr = getCommonString(key: "Kr_key")
        lblPrice.text = "\(kr) \(data["price"].stringValue)"
        let isSelected = data["is_selected"].boolValue
        if isSelected{
            imgCheckmark.isHidden = false
        }else{
            imgCheckmark.isHidden = true
        }
        
    }

}
