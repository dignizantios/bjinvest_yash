//
//  OtherPropertySelectiingCell.swift
//  BJInvest
//
//  Created by Haresh Bhai on 05/01/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import UIKit
import SwiftyJSON

class OtherPropertySelectiingCell: UITableViewCell {
    @IBOutlet weak var btnChekUncheck: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblBottom: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblTitle.textColor = UIColor.appThemeLightGrayColor
        lblTitle.font = themeFont(size: 16, fontname: .semibold)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setData (data:JSON)
    {
        lblTitle.text = data["title"].stringValue
        let isSelected = data["isSelected"].boolValue
        if isSelected{
            btnChekUncheck.setImage(UIImage(named: "checkbox_checked.png"), for: .normal)
        }else{
            btnChekUncheck.setImage(UIImage(named: "checkbox_unchecked.png"), for: .normal)
        }
        
    }

}
