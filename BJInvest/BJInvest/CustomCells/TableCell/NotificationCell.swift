//
//  NotificationCell.swift
//  BJInvest
//
//  Created by Haresh Bhai on 08/01/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import UIKit
import SwiftyJSON

class NotificationCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblTitle.textColor = UIColor.black
        lblTitle.font = themeFont(size: 18, fontname: .regular)
        lblDate.textColor = UIColor.appThemeLightGrayColor
        lblDate.font = themeFont(size: 16, fontname: .regular)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setData(data:JSON)
    {
        lblTitle.text = data["title"].stringValue
        lblDate.text = data["date"].stringValue
    }

}
